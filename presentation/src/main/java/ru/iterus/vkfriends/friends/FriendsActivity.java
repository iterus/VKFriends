package ru.iterus.vkfriends.friends;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ru.iterus.vkfriends.R;
import ru.iterus.vkfriends.databinding.ActivityFriendsBinding;

public class FriendsActivity extends AppCompatActivity {

    private ActivityFriendsBinding layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layout = DataBindingUtil.setContentView(this, R.layout.activity_friends);
    }
}
