package ru.iterus.vkfriends.auth;

interface AuthContract {

    interface View {

        void loadUrl(String url);
    }

    interface Presenter {

        void onUrlLoad(String url);
    }
}
