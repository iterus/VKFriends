package ru.iterus.vkfriends.auth;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class AuthPresenter implements AuthContract.Presenter {

    private final AuthContract.View view;
    private final String redirectUrl = "https://oauth.vk.com/blank.html";
    private final String authUrl = "https://oauth.vk.com/authorize?" +
            "client_id=6207699" +
            "&display=page" +
            "&redirect_uri=" + redirectUrl +
            "&scope=friends" +
            "&response_type=token" +
            "&v=5.75";

    AuthPresenter(AuthContract.View view) {
        this.view = view;

        view.loadUrl(authUrl);
    }

    @Override
    public void onUrlLoad(String url) {
        if (url.startsWith(redirectUrl)) {
            // extract token
            Pattern p = Pattern.compile("access_token=(.*?)&");
            Matcher m = p.matcher(url);

            if (m.find()) {
                String token = m.group(1);

            } else {
                // Token extraction failed
            }
        }
    }
}
