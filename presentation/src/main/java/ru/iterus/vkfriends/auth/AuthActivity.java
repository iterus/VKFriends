package ru.iterus.vkfriends.auth;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ru.iterus.vkfriends.R;
import ru.iterus.vkfriends.databinding.ActivityAuthBinding;

public class AuthActivity extends AppCompatActivity
        implements AuthContract.View {

    private ActivityAuthBinding layout;

    AuthContract.Presenter presenter;
    AuthWebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layout = DataBindingUtil.setContentView(this, R.layout.activity_auth);

        // should do dependency injection
        presenter = new AuthPresenter(this);
        webView = new AuthWebView(presenter);

        layout.loginView.setWebViewClient(webView);
    }

    @Override
    public void loadUrl(String url) {
        layout.loginView.loadUrl(url);
    }
}
