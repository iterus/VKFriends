package ru.iterus.vkfriends.mvp;

public abstract class BasePresenter<V> implements IBasePresenter<V> {

    protected V view;

    protected BasePresenter() {
    }

    public void attachView(V view) {
        this.view = view;
    }

    public void detachView() {
        view = null;
    }
}
