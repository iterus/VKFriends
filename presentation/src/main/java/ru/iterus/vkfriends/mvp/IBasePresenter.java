package ru.iterus.vkfriends.mvp;

public interface IBasePresenter<V> {

    void attachView(V view);

    void detachView();
}
