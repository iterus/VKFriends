package ru.iterus.vkfriends.model;

class Friend {
    int id;

    String firstName;
    String lastName;

    Friend(int id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
